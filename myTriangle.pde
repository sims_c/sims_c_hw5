// Courtney Sims
// CS 4300 - Homework #5
// Polygon Mesh
// due: 11/10/2011, 9pm


// Represents a triangle which is drawn on the screen
// and colored using Barycentric interpolation.
class myTriangle {
  PVector vertex0;
  PVector vertex1;
  PVector vertex2;
  PVector color0;
  PVector color1;
  PVector color2;

  // Constructs a triangle with the given vertices and colors.
  myTriangle(PVector v0, PVector v1, PVector v2, PVector c0, PVector c1, PVector c2) {
    this.vertex0 = v0;
    this.vertex1 = v1;
    this.vertex2 = v2;
    this.color0 = c0;
    this.color1 = c1;
    this.color2 = c2;
  }

  // Constructs a triangle with no specified vertices or colors.
  myTriangle() {
    this.vertex0 = new PVector();
    this.vertex1 = new PVector();
    this.vertex2 = new PVector();
    this.color0 = new PVector();
    this.color1 = new PVector();
    this.color2 = new PVector();
  }


  // Translates a vertex so the origin is in the
  // center of the screen.  Also makes the up direction
  // positive Y and the down direction negative Y.
  PVector centerVertexOrigin(PVector v) {
    return new PVector(v.x + width/2, -1 * v.y + (height/2), v.z);
  }


  // If a color component is < 0, makes it 0.  If a color
  // component is > 1, makes it 1.
  PVector fixColorComponent(PVector c) {
    float[] colorArray = new float[0];
    colorArray = append(colorArray, c.x);
    colorArray = append(colorArray, c.y);
    colorArray = append(colorArray, c.z);

    for (int i = 0; i < colorArray.length; i++) {
      if (colorArray[i] < 0) {
        colorArray[i] = 0;
      } 
      else if (colorArray[i] > 1) {
        colorArray[i] = 1;
      }
    }

    return new PVector(colorArray[0], colorArray[1], colorArray[2]);
  }


  // Fixes all color components of this triangle.
  void fixAllColorComponents() {
    this.color0 = fixColorComponent(this.color0);
    this.color1 = fixColorComponent(this.color1);
    this.color2 = fixColorComponent(this.color2);
  }


  // Vector used to check if this triangle is degenerate or
  // specified in CCW order.
  PVector getN() {
    PVector tempV0 = this.centerVertexOrigin(this.vertex0);
    PVector tempV1 = this.centerVertexOrigin(this.vertex1);
    PVector tempV2 = this.centerVertexOrigin(this.vertex2);
    PVector n = (PVector.sub(tempV2, tempV0)).cross(PVector.sub(tempV1, tempV0));
    return n;
  }


  // Checks whether this triangle is degenerate.
  boolean isDegenerate() {
    PVector n = this.getN();
    float nMag = n.mag();

    return (abs(nMag - 0) < 0.01);
  }


  // Checks whether this triangle is specified in counter-clockwise order.
  boolean isCCW() {
    PVector n = this.getN();
    PVector unit = new PVector(0, 0, 1);
    float checkN = n.dot(unit);

    return (checkN > 0);
  }

  // Used to compute color when flat-shading this triangle.
  float getD() {
    PVector n = this.getN();
    float nMag = n.mag();
    PVector unitN = PVector.div(n, nMag);
    PVector unit = new PVector(0, 0, 1);
    float d = unitN.dot(unit);

    return d;
  }


  // is this triangle front-facing?
  boolean isFrontFacing() {
    return !(this.getD() < 0);
  }


  // The function used in calculating alpha, beta, and gamma.
  float getF(PVector vec1, PVector vec2, float x, float y) {
    float f = ((vec1.y - vec2.y)*x) + ((vec2.x - vec1.x)*y) + (vec1.x * vec2.y) - (vec2.x * vec1.y);
    return f;
  }


  // Returns alpha, beta, and gamma at the given point (x, y)
  // as a PVector.
  PVector getBarycentricCoords(float x, float y) {
    PVector tempV0 = this.centerVertexOrigin(this.vertex0);
    PVector tempV1 = this.centerVertexOrigin(this.vertex1);
    PVector tempV2 = this.centerVertexOrigin(this.vertex2);

    float baryAlpha = getF(tempV1, tempV2, x, y) / getF(tempV1, tempV2, tempV0.x, tempV0.y);
    float baryBeta = getF(tempV2, tempV0, x, y) / getF(tempV2, tempV0, tempV1.x, tempV1.y);
    float baryGamma = getF(tempV0, tempV1, x, y) / getF(tempV0, tempV1, tempV2.x, tempV2.y);

    return new PVector(baryAlpha, baryBeta, baryGamma);
  }


  // Is the given number between 0 and 1?
  boolean betweenZeroOne(float num) {
    return ((num >= 0) && (num <= 1));
  }


  // Colors pixels according to their barycentric coordinates
  // and whether they are inside the triangle.  Colors according
  // to the given hwNumber ("4" or "5b") to determine whether or
  // not to do flat shading with a directional light source.
  void colorPixels(String hwNumber) {
    float d = this.getD();

    for (int x = 0; x < width; x ++) {
      for (int y = 0; y < height; y++) {

        PVector baryCoords = this.getBarycentricCoords(x, y);

        float baryAlpha = baryCoords.x;
        float baryBeta = baryCoords.y;
        float baryGamma = baryCoords.z;

        if (betweenZeroOne(baryAlpha)
          && betweenZeroOne(baryBeta)
          && betweenZeroOne(baryGamma)) {

          if (hwNumber.equals("4")) {
            PVector v0c = PVector.mult(this.color0, baryAlpha);
            PVector v1c = PVector.mult(this.color1, baryBeta);
            PVector v2c = PVector.mult(this.color2, baryGamma);
            PVector colorVector = PVector.add(v0c, PVector.add(v1c, v2c));
            color colorAtPoint = color(colorVector.x, colorVector.y, colorVector.z);

            stroke(colorAtPoint);
            // Treats the z coordinate as 0 for this assignment.
            point(x, y, 0);
          }
          else if (hwNumber.equals("5b")) {
            PVector tempC0 = PVector.mult(this.color0, d);
            PVector tempC1 = PVector.mult(this.color1, d);
            PVector tempC2 = PVector.mult(this.color2, d);
            PVector v0c = PVector.mult(tempC0, baryAlpha);
            PVector v1c = PVector.mult(tempC1, baryBeta);
            PVector v2c = PVector.mult(tempC2, baryGamma);
            PVector colorVector = PVector.add(v0c, PVector.add(v1c, v2c));
            color colorAtPoint = color(colorVector.x, colorVector.y, colorVector.z);

            stroke(colorAtPoint);
            point(x, y, 0);
          }
        }
      }
    }
  }


  // Rotates this triangle by the given angle in radians around the Y axis.
  void rotateAroundY(float angle) {
    float v0x = this.vertex0.x;
    float v1x = this.vertex1.x;
    float v2x = this.vertex2.x;
    this.vertex0.x = (sin(angle) * this.vertex0.z) + (cos(angle) * this.vertex0.x);
    this.vertex0.z = (cos(angle) * this.vertex0.z) - (sin(angle) * v0x);
    this.vertex1.x = (sin(angle) * this.vertex1.z) + (cos(angle) * this.vertex1.x);
    this.vertex1.z = (cos(angle) * this.vertex1.z) - (sin(angle) * v1x);
    this.vertex2.x = (sin(angle) * this.vertex2.z) + (cos(angle) * this.vertex2.x);
    this.vertex2.z = (cos(angle) * this.vertex2.z) - (sin(angle) * v2x);
  }


  // Rotates this triangle by the given angle in radians around the X axis.
  void rotateAroundX(float angle) {
    float v0y = this.vertex0.y;
    float v1y = this.vertex1.y;
    float v2y = this.vertex2.y;
    this.vertex0.y = (cos(angle) * this.vertex0.y) - (sin(angle) * this.vertex0.z);
    this.vertex0.z = (sin(angle) * v0y) + (cos(angle) * this.vertex0.z);
    this.vertex1.y = (cos(angle) * this.vertex1.y) - (sin(angle) * this.vertex1.z);
    this.vertex1.z = (sin(angle) * v1y) + (cos(angle) * this.vertex1.z);
    this.vertex2.y = (cos(angle) * this.vertex2.y) - (sin(angle) * this.vertex2.z);
    this.vertex2.z = (sin(angle) * v2y) + (cos(angle) * this.vertex2.z);
  }


  // Scales this triangle by the given decimal.
  void scaleTriangle(float scaleDiff) {
    this.vertex0.x = scaleDiff * this.vertex0.x;
    this.vertex0.y = scaleDiff * this.vertex0.y;
    this.vertex0.z = scaleDiff * this.vertex0.z;
    this.vertex1.x = scaleDiff * this.vertex1.x;
    this.vertex1.y = scaleDiff * this.vertex1.y;
    this.vertex1.z = scaleDiff * this.vertex1.z;
    this.vertex2.x = scaleDiff * this.vertex2.x;
    this.vertex2.y = scaleDiff * this.vertex2.y;
    this.vertex2.z = scaleDiff * this.vertex2.z;
  }


  // Transforms this triangle according to the keypress.
  // Left and right arrow keys rotate around the Y axis.
  // Up and down arrow keys rotate around the X axis.
  // The 'i' key zooms in, and the 'o' key zooms out.
  // This function assumes that the angle will be given in positive
  // degrees (not radians), and that the scale factor will be 0 > x > 1.
  void transformTriangle(float angleInDegrees, float scaleDiff) {
    float rad = radians(angleInDegrees);

    if (keyPressed) {
      // Rotates left around the Y axis
      if (keyCode == LEFT && key == CODED) {
        rotateAroundY(-1 * rad);
      }
      // Rotates right around the Y axis
      else if (keyCode == RIGHT && key == CODED) {
        rotateAroundY(rad);
      }
      // Rotates up around the X axis
      else if (keyCode == UP && key == CODED) {
        rotateAroundX(-1 * rad);
      }
      // Rotates down around the X axis
      else if (keyCode == DOWN && key == CODED) {
        rotateAroundX(rad);
      }
      // Zooms in
      else if (key == 'i' || key == 'I') {
        this.scaleTriangle(scaleDiff + 1);
      }
      // Zooms out
      else if (key == 'o' || key == 'O') {
        this.scaleTriangle(1 - scaleDiff);
      }
    }
  }


  // draws a wireframe rendering of this triangle in random colors
  void drawWireframe() {
    PVector tempV0 = this.centerVertexOrigin(this.vertex0);
    PVector tempV1 = this.centerVertexOrigin(this.vertex1);
    PVector tempV2 = this.centerVertexOrigin(this.vertex2);

    stroke(random(0, 1), random(0, 1), random(0, 1));
    line(tempV0.x, tempV0.y, 0, tempV1.x, tempV1.y, 0);
    stroke(random(0, 1), random(0, 1), random(0, 1));
    line(tempV1.x, tempV1.y, 0, tempV2.x, tempV2.y, 0);
    stroke(random(0, 1), random(0, 1), random(0, 1));
    line(tempV2.x, tempV2.y, 0, tempV0.x, tempV0.y, 0);
  }
  
  
  // flat shading of this triangle using Processing's built-in methods.
  void flatShade() {
    PVector tempV0 = this.centerVertexOrigin(this.vertex0);
    PVector tempV1 = this.centerVertexOrigin(this.vertex1);
    PVector tempV2 = this.centerVertexOrigin(this.vertex2);
    beginShape();
    fill(this.color0.x, this.color0.y, this.color0.z); 
    vertex(tempV0.x, tempV0.y, tempV0.z);
    fill(this.color1.x, this.color1.y, this.color1.z); 
    vertex(tempV1.x, tempV1.y, tempV1.z);
    fill(this.color2.x, this.color2.y, this.color2.z); 
    vertex(tempV2.x, tempV2.y, tempV2.z);
    endShape(CLOSE);
  }
}

