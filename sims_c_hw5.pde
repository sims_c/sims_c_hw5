// Courtney Sims
// CS 4300 - Homework #5
// Polygon Mesh
// due: 11/10/2011, 9pm


// Note: the program will successfully read in and draw any number
// of triangles specified on multiple lines (1 triangle = 1 line).
// It currently throws exceptions if the input file has any empty
// lines, but it still draws the right specified triangles.  It will
// need the correct number of whitespace-separated elements (18) on
// each line, otherwise it will throw an error.  Additionally, it
// assumes that every line of input will *always* be given in the
// following format:
// x0 y0 z0 x1 y1 z1 x2 y2 z2 r0 g0 b0 r1 g1 b1 r2 g2 b2
// Triangles whose vertices are specified in clockwise order, as well
// as degenerate triangles, will be ignored and not drawn by the program.
// Lines that begin with # will also be ignored.



// Declare variables.
String loadPath;
String[] lines1;
String[] lines2;
float[][] lines3;
myTriangle[] triangleList;
String homeworkNumber;
static final float SCALE_DIFF = 0.05;
static final float ROTATION_DEGREES = 5.0;


// Initialize variables.
void setup() {
  /**********************
   SET HOMEWORK NUMBER HERE: options are "4", "5a", or "5b".
   (or if you'd like flat shading using Processing's built-in functions, use the "processingEasyMode" option)
   **********************/
  homeworkNumber = "5a";
  /**********************/
  /**********************/

  size(512, 512, P3D);
  colorMode(RGB, 1.0);
  noStroke();

  lines2 = new String[0];
  lines3 = new float[0][0];
  triangleList = new myTriangle[0];

  // Open a GUI file chooser to select the path to a file.
  loadPath = selectInput();

  if (loadPath == null) {
    println("No file was selected.");
  }
  else {

    // Load every line from the file at the given path, remove
    // comment lines, split by whitespace and convert to float.
    lines1 = loadStrings(loadPath);
    lines2 = removeCommentLines(lines1);
    lines3 = splitAndMakeFloat(lines2);

    // Split the array containing arrays of floats into 2 arrays,
    // one array containing arrays of ver
    for (int i = 0; i < lines3.length; i++) {
      triangleList = (myTriangle[]) append(triangleList, makeTriangle(lines3[i]));
    }
  }
}


// Displays shapes on the screen.
void draw() {

  smooth();
  background(0, 0, 0);
  
  if (homeworkNumber.equals("processingEasyMode")) {
    lights();
  }

  for (int i = 0; i < triangleList.length; i++) {
    // Only draw the triangle if it's not degenerate and is
    // defined in CCW order.
    if ((!triangleList[i].isDegenerate()) && triangleList[i].isCCW()) {
      if (homeworkNumber.equals("4") || (homeworkNumber.equals("5b") && triangleList[i].isFrontFacing())) {
        triangleList[i].colorPixels(homeworkNumber);
      }
      else if (homeworkNumber.equals("5a")) {
        triangleList[i].drawWireframe();
      }
      else if (homeworkNumber.equals("processingEasyMode")) {
        triangleList[i].flatShade();
      }
    }
  }
}


// Returns a new String array containing all the lines
// from the given String array *except* the ones
// beginning with # .
String[] removeCommentLines(String[] someLines) {
  String[] newLines = new String[0];

  for (int i=0; i < someLines.length; i++) {    
    if (!someLines[i].startsWith("#")) {
      newLines = append(newLines, someLines[i]);
    }
  }

  return newLines;
}


// For each line in the given array, splits the
// elements by whitespace and converts to floats.
// Returns an array of arrays. E.g. the array
// ["1 2 3", "4 5 6"] would become [[1, 2, 3], [4, 5, 6]].
float[][] splitAndMakeFloat(String[] someLines) {
  float[][] newLines = new float[0][0];

  for (int i=0; i < someLines.length; i++) {
    newLines = (float[][]) append(newLines, float(splitTokens(someLines[i])));
  }

  return newLines;
}


// Makes triangles from a line with the following format:
// [x0 y0 z0 x1 y1 z1 x2 y2 z2 r0 g0 b0 r1 g1 b1 r2 g2 b2]
myTriangle makeTriangle(float[] aLine) {

  myTriangle aTriangle = new myTriangle();

  // Throw an exception if the length of the line is incorrect.
  if (aLine.length != 18) {
    try {
      lengthException();
    }
    catch (Exception e) {
      println(e.getMessage());
    }
  }
  else {

    PVector v0 = new PVector(aLine[0], aLine[1], aLine[2]);
    PVector v1 = new PVector(aLine[3], aLine[4], aLine[5]);
    PVector v2 = new PVector(aLine[6], aLine[7], aLine[8]);

    PVector c0 = new PVector(aLine[9], aLine[10], aLine[11]);
    PVector c1 = new PVector(aLine[12], aLine[13], aLine[14]);
    PVector c2 = new PVector(aLine[15], aLine[16], aLine[17]);

    aTriangle = new myTriangle(v0, v1, v2, c0, c1, c2);
    aTriangle.fixAllColorComponents();
  }

  return aTriangle;
}


// Used to catch errors if an array has the incorrect length.
void lengthException() throws Exception {
  throw new Exception("Please give an array of the correct length.");
}


// Rotates/scales the triangles on appropriate key press.
// (see transformTriangle() method in myTriangle class).
void keyPressed() {
  if (homeworkNumber != "4") {
    for (int i = 0; i < triangleList.length; i++) {
      triangleList[i].transformTriangle(ROTATION_DEGREES, SCALE_DIFF);
    }
  }
}

